# opensearch

| Node Group | Deployment | Environments | Namespace  |
| ---------- | ---------- | ------------ | ---------- |
| stateful-opensearch  | Argo CD    | Sandbox, Production   | opensearch |

## Description

Helm chart for OpenSearch. Used to index posts among other things.

## Deploy

Deployed using Argo CD. View the application in Argo, and "Sync" when changes are in master.

## Private Registry

As this module is privated, a secret is needed for credentials in order to deploy onto a new cluster:

https://kubernetes.io/docs/tasks/configure-pod-container/pull-image-private-registry/

## Re-indexing

```json
POST _reindex
{
  "source": {
    "remote": {
      "host": "https://a86946c900d224f13a04df1953b08456-b6b91d224423e551.elb.us-east-1.amazonaws.com:9200",
      "username": "<<>>",
      "password": "<<>>"
    },
    "index": "minds-search-activity"
  },
  "dest": {
    "index": "minds-search-activity"
  }
}
```
